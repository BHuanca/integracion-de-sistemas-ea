# Lab. 06: MÓDULO COMPRAS 

## Desarrollo

2.0 Creación de Solicitud de presupuesto
- #### Imagen del correo enviado
<img src="imagenes/2.4.PNG" width="520" name="Correo enviado">
<br>

- #### Imagen de la solicitud validada
<img src="imagenes/2.6.PNG" width="520" name="Solicitud validada">
<br>

3.0 Recepción de compras
- #### Imagen de la solicitud del presupuesto con las entregas dadas
<img src="imagenes/3.3.PNG" width="520" name="Solicitud de presupuesto">
<br>

4.0 Facturación de proveedores
- #### Imagen de Otra informacion de la factura
<img src="imagenes/4.1.PNG" width="520" name="Otra informacion">
<br>

- #### Vamos al campo Proveedores del menu lateral y seleccionamos el proveedor
<img src="imagenes/4.3.1.PNG" width="520" name="Seleccionar proveedor">
<br>

- #### Primero creamos el banco con su codigo SWIFT
<img src="imagenes/4.3.2.1.PNG" width="520" name="Crear banco">
<br>

- #### Segundo ponemos el numero de cuenta que tiene la empresa
<img src="imagenes/4.3.2.2.PNG" width="520" name="Numero de cuenta">
<br>

- #### Tercero estara creada la cuenta bancaria
<img src="imagenes/4.3.2.3.PNG" width="520" name="Creada cuenta bancaria">
<br>

- #### Imagen de la factura pagada
<img src="imagenes/4.4.PNG" width="520" name="Factura pagada">
<br>
    
5.0 Tarifas de proveedores
- #### Imagen de las tarfifas del proveedor
<img src="imagenes/5.3.PNG" width="520" name="Tarifa del proveedor">
<br>

6.0 Licitaciones
- #### Imagen del acuerdo de compra
<img src="imagenes/6.3.PNG" width="520" name="Acuerdo de compra">
<br>

- #### Dentro de estos tenemos que es escojer el cual nos convenga
<img src="imagenes/6.7.1.PNG" width="520" name="Solicitudes enviadas">
<br>

- #### Despues confirmamos el pedido del que escojamos y a los demas lo cancelamos
<img src="imagenes/6.7.2.PNG" width="520" name="Confirmar y cancelar">
<br>

- #### Una vez hecho eso, tenemos validar el pedido para despues facturar
<img src="imagenes/6.7.3.PNG" width="520" name="Validar pedido">
<br>

- #### Cuando esta validado recien podremos facturar el pedido, luego tenemos que validarlo y registrar el pago
<img src="imagenes/6.7.4.PNG" width="520" name="Facturar y registrar pago">
<br>

7.0 Módulos de terceros
- #### Imagen del modulo Top Buying Products en accion
<img src="imagenes/7.8.PNG" width="520" name="Modulo instalado">
<br>

- #### Imagen del modulo Toponimos de Peru instalado
<img src="imagenes/7.9.1.PNG" width="520" name="Modulo instalado">
<br>

- #### Este modulo hace que podamos seleccionar pais, departamento, provincia y distrito
<img src="imagenes/7.9.2.PNG" width="520" name="Accion del modulo">
<br>

## Tarea

- Se completó el desarrollo del laboratorio.

## Conclusiones

- Al hacer un cambio en la configuracion del idioma, debemos de cargar neuvamente para que se muestren estos
- Se puede hacer varios tipos de entrega como pagos, por ejemplo seria la entrega parcial por cantidad
- Cada empresa tiene su propia tarifa por ello podemos verla habilitando la opcion tarifas de compras de compras
- Odoo nos ayuda a identificar los diferentes tartifas de las empresas para que podamos escojer la mas conveniente
- Podemos instalar modulos de terceros por diferentes carpetas porque Odoo nos permite hacer esto, separando todo por una coma