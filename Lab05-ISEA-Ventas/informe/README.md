# Lab. 05: MÓDULO VENTAS

## Desarrollo

2.0 Creación de Cotización a cliente
- #### Imagen de la cotización realizada
<img src="imagenes/2.6.PNG" width="500" name="Cotizacion realizada">
<br>

- #### Imagen de la plantilla del mensaje
<img src="imagenes/2.8.PNG" width="500" name="Plantilla de mensaje">
<br>

- #### Imagen de la confirmacion de venta
<img src="imagenes/2.8.1.PNG" width="500" name="Confirmacion de venta">
<br>

3.0 Entrega de productos de una Cotización 
- #### Imagen de los productos aumentados a 500 unidades
<img src="imagenes/3.2.PNG" width="500" name="Productos aumentados">
<br>

- #### Imagen de Comprobante de disponibilidad
<img src="imagenes/3.2.1.PNG" width="500" name="Comprobante disponibilidad">
<br>

4.0 Facturación y registro de pago de una Cotizació
- #### Imagen del borrador de la factura
<img src="imagenes/4.4.PNG" width="500" name="Borrador factura">
<br>

- #### Imagen de la factura pendiente
<img src="imagenes/4.6.PNG" width="500" name="Factura Pendiente">
<br>

5.0 Configuración de envío de Correos 
- #### Imagen de la conexion correcta
<img src="imagenes/5.3.PNG" width="500" name="Conexion correcta">
<br>

- #### Imagen del correo recibido
<img src="imagenes/5.4.PNG" width="500" name="Correo recibido">
<br>

6.0 Cambio de secuencia
- #### Imagen de la secuencia modificada
<img src="imagenes/6.3.PNG" width="500" name="Secuencia modificada">
<br>

- #### Imagen de una nueva cotizacion creada
<img src="imagenes/6.4.PNG" width="500" name="Nueva cotizacion">
<br>

7.0 Listas de precios
- #### Imagen de la Lista de Precio modificada
<img src="imagenes/7.3.PNG" width="500" name="Precio modificado">
<br>

8.0 Portal del cliente 
- #### Imagen del mensaje enviado
<img src="imagenes/8.4.PNG" width="500" name="Mensaje enviado">
<br>

- #### Imagen de los pedidos realizados
<img src="imagenes/8.6.PNG" width="500" name="Pedidos realizados">
<br>

## Tarea
1.0 Crear una cotización, validarla y registrar una factura por pago previo (La cotización será de S/1000.00 y la primera factura será de S/200.00, la otra por el restante)
- #### Imagen de la cotizacion creada y validada
<img src="imagenes/tarea/1.0.PNG" width="500" name="Cotizacion creada">
<br>

2.0 Adjunte una captura del proceso, de cómo verá el cliente mediante su portal web a la factura, al inicio de ser emitida, luego del primer pago, y finalmente cuando está pagada totalmente
- #### Tiene que entrar a facturas y seleccionar la factura que quiere ver
<img src="imagenes/tarea/2.1.PNG" width="500" name="Factura creada">
<br>

- #### Cuando haya hecho la primera paga se vera lo siguiente
<img src="imagenes/tarea/2.2.PNG" width="500" name="Pago previo">
<br>

- #### Cuando haya hecho todo el pago se vera lo siguiente
<img src="imagenes/tarea/2.3.PNG" width="500" name="Paga completo">
<br>

## Conclusiones
- Odoo identifica si un cliente no tiene correo por ello, nosotros podemos ponerlo para que no haya errores
- Para un correo a otros, es necesario configurar el servidor SMTP del correo, en este caso usamos el de gmail
- Antiguamente no podiamos enviar un correo a nosotros mismo, pero hoy es posible hacerlo
- Los pagos se pueden dar por partes configurando pagos adelantados, previos o fijos
- Los correos a quien estamos creando una cuenta deben ser unicos, no se puede enviar una solicitud de acceso a 2 o mas correos iguales
