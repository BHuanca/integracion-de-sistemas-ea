# Lab. 3: MÓDULO LOGÍSTICO 

## Desarrollo

2.0 Configuración de Almacenes
- #### Imagen del almacen creado
<img src="imagenes/2.1.PNG" width="500" name="Almacen creado">
<br>

4.0 Gestión de Productos
- #### Imagen del producto manzana creado
<img src="imagenes/4.1.PNG" width="500" name="Manzana creada">
<br>

- #### Imagen de las cantidades disponibles
<img src="imagenes/4.2.PNG" width="500" name="Cantidades Disponibles">
<br>

5.0 Importación y exportación masiva de productos
- #### Imagen de la importacion de los productos
<img src="imagenes/5.0.PNG" width="500" name="Importacion de productos">
<br>

6.0 Ajustes de inventarios.
- #### Imagen del inventario validado
<img src="imagenes/6.1.PNG" width="500" name="Inventario validado">
<br>

## Tarea
9.1. Habilite las opciones Atributos y variantes, y Unidades de medida. Vaya a la ficha de producto e indique las diferencias, así como adjunte imágenes de productos con varios atributos y/o unidades de medida distintas.
```
Atributos y variantes: Si no la tenemos activada, no nos habilitara una sección de variantes, esta sección permite establecer los atributos del producto como color, tamaño, etc
Unidades de medida: Si no está activado, no nos aparecerá las unidades de medida que tiene cada producto, este nos permite vender y comprar productos con diferentes unidades de medida
```

9.2. Entre al menú Reglas de abastecimiento e intente crear una. Indique en que caso puede servir este proceso.
- #### Producto Fresa - Variantes
<img src="imagenes/tarea/9.2.1.PNG" width="500" name="Fresa - Variantes">
<br>

- #### Producto Fresa - Atributos
<img src="imagenes/tarea/9.2.1.1.PNG" width="500" name="Fresa - Atributos">
<br>

- #### Producto Palta - Variantes
<img src="imagenes/tarea/9.2.2.PNG" width="500" name="Fresa - Variantes">
<br>

- #### Producto Palta - Atributos
<img src="imagenes/tarea/9.2.2.1.PNG" width="500" name="Fresa - Atributos">
<br>

## Conclusiones

- Se puede crear y cambiar la base de datos al entrar a la página web
- Podemos crear varios almacenes seleccionando la opción multialmacen, en este caso odoo solo toma una ubicación
- Cuando creamos un almacén se crean por defecto rutas que podemos modificarlas o verlas
- Odoo tiene una opción de movimiento de producto que nos permite ver la ubicación de este y hacia donde fue movido
- Podemos crear varios productos y asignarles su precio, cantidad, costo, etc, además podemos asignarle sus atributos habilitando la opción "atributos y variables"

