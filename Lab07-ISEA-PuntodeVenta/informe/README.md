# Lab07. 06: MÓDULO PUNTO DE VENTA

## Desarrollo

2.0 Funcionamiento del Punto de Venta
- #### GIF Creacion de un nuevo cliente y seleccion
<img src="imagenes/2.3.gif" width="520" name="Creacion del cliente">
<br>

- #### Imagen del comprobante de pago
<img src="imagenes/2.5.PNG" width="520" name="Comprobante de pago">
<br>

- #### Imagen de la sesion validada y cerrada
<img src="imagenes/2.6.PNG" width="520" name="Sesion validada">
<br>

3.0 Añadiendo control al POS
- #### 3.4.1 Combinacion efectivo, tarjeta, tarjeta
<img src="imagenes/3.4.1.PNG" width="520" name="Combinacion01">
<br>

- #### 3.4.2 Combinacion tarjeta, tarjeta, efectivo
<img src="imagenes/3.4.2.PNG" width="520" name="Combinacion02">
<br>

- #### 3.6 Imagen de la ventana de seleccion de usuario
<img src="imagenes/3.6.PNG" width="520" name="Ventana seleccion">
<br>

- #### 3.8 Imagen de la ventana para poner la clave
<img src="imagenes/3.8.PNG" width="300" name="Ventana clave">
<br>

4.0 Categorías de Productos
- #### 4.5.1 Imagen de la categoria Frutas
<img src="imagenes/4.5.1.PNG" width="520" name="Categoria frutas">
<br>

- #### 4.5.2 Imagen de la categoria Verduras
<img src="imagenes/4.5.2.PNG" width="520" name="Categoria verduras">
<br>

5.0 Configuración de restaurant
- #### 5.1 Imagen del nuevo punto de venta configurado
<img src="imagenes/5.1.PNG" width="520" name="Punto venta configurado">
<br>

- #### 5.3.1 Imagenes de agregar y sacar dinero
<img src="imagenes/5.3.1.PNG" width="520" name="Agregar y sacar dinero">
<br>

- #### 5.3.2 Imagen del balance de cierre
<img src="imagenes/5.3.2.PNG" width="520" name="Balance cierre">
<br>

- #### 5.5 GIF Manejo de pedidos y cobrado
<img src="imagenes/5.5.gif" width="520" name="Pedidos y cobrado">
<br>

- #### 5.7 GIF Mostrando el boton transferir
<img src="imagenes/5.7.gif" width="520" name="Boton transferir">
<br>

## Tarea 

- #### 6.1 GIF Mostrando la funcionalidad del modulo pos_loyalty
<img src="imagenes/6.1.gif" width="520" name="Funcionalidad pos_loyalty">
<br>

## Conclusiones

- La interfaz del módulo punto de venta es muy facil de usar y cualquiera puede usarlo
- Todas las configuraciones de punto de venta se pueden hacer en configuraciones como agregar nuevos métodos de pagos
- También se puede agregar una contraseña a cada tipo de cajero que exista en punto de venta
- Crear y diseñar nuestro propio restaurante es muy fácil con las herramientas que nos brinda para adecuarse a todo tipo de local
- Podemos también agregar nuevos módulos para que nos ayuden a implementar nuevas cosas

