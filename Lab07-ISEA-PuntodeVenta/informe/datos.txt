Lab07 - MÓDULO PUNTO DE VENTA

2.0 Funcionamiento del Punto de Venta
    GIF de la creacion de un nuevo cliente y seleccion 2.3
    Imagen del comprobante de pago 2.5
    Imagen de la sesion validada y cerrada 2.6

3.0 Añadiendo control al POS
    3.4.1 Combinacion efectivo, tarjeta, tarjeta
    3.4.2 Combinacion tarjeta, tarjeta, efectivo
    3.6 Imagen de la ventana de seleccion de usuario
    3.8 Imagen de la vetana para poner la clave

4.0 Categorías de Productos
    4.5.1 Imagen de la categoria Frutas
    4.5.2 Imagen de la categoria Verduras

5.0 Configuración de restaurant
    5.1 Imagen del nuevo punto de venta configurado
    5.3.1 Imagenes de agregar y sacar dinero
    5.3.2 Imagen del balance de cierre
    5.5 GIF Manejo de pedidos y cobrado
    5.7 GIF Mostrando el boton transferir

6.0 Tarea 
    6.1 GIF Mostrando la funcionalidad del modulo pos_loyalty

