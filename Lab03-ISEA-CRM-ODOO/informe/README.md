# Lab. 3: CRM EN ODOO

## Desarrollo

3.0 Configuracion ODOO CRM - Planificador de ventas
- #### Imagen de los Objetivos
<img src="imagenes/3.1.PNG" width="500" name="Imagen de los Objetivos">
<br>

- #### Imagen de los KPIs de estrategia
<img src="imagenes/3.2.PNG" width="500" name="KPIs de estrategia">
<br>

- #### Imagen del flujo del trabajo
<img src="imagenes/3.3.PNG" width="500" name="Oportunidades">
<br>

4.0 Configuracion ODOO CRM - Clientes
- #### Imagen del cliente creado
<img src="imagenes/4.1.PNG" width="500" name="Cliente creado">
<br>

5.0 Equipo de Ventas - Equipo de ventas directas
- #### Imagen del cliente creado
<img src="imagenes/5.1.PNG" width="500" name="Miembros agregados">
<br>

6.2 Adicionar manualmente un contacto
- #### Imagen del contacto generado
<img src="imagenes/6.2.PNG" width="500" name="Contacto generado">
<br>

6.3. Crear una nueva oportunidad
- #### Imagen de la oportunidad creada
<img src="imagenes/6.3.PNG" width="500" name="Oportunidad creada">
<br>

7.0 Generar iniciativas desde el sitio web de ODOO
- #### Imagen del cover configurado
<img src="imagenes/7.1.PNG" width="500" name="Cover configurado">
<br>

- #### Imagen del mensaje de confirmacion enviado
<img src="imagenes/7.2.PNG" width="500" name="Mensaje de confirmacion">
<br>

## Ejercicio
- #### Contacto 01 - Informacion
<img src="imagenes/Ejercicio/1.0.PNG" width="500" name="Contacto informacion 01">
<br>

- #### Contacto 02 - Informacion
<img src="imagenes/Ejercicio/1.1.PNG" width="500" name="Contacto informacion 02">
<br>

- #### Imagen de las nuevas oportunidades
<img src="imagenes/Ejercicio/1.2.PNG" width="500" name="Oportunidades creadas">
<br>

## Tarea
1.0 Procedimiento para la importacion de un cliente en Odoo
```
Procedimiento para la importacion de datos en Odoo
1. Primero debemos ver la estructura de los datos que tenemos en la bd, para esto debemos exportar una prueba
   de la seccion de clientes.
2. Una vez visto los datos, tenemos que crear los datos que queramos importar y convertirlo en formato CSV
3. Entramos a la pagina de odoo en la seccion de clientes hacemos click en el boton "importar", despues nos mostrara
   la estructura de la importacion, el separador tiene que ser "Punto y coma", y hacemos click en el boton importar
4. Se nos importara correctamente, despues lo abrimos
```
- #### Informacion para insertar en exel
<img src="imagenes/Tarea/1.0.PNG" width="500" name="Exel - informacion">
<br>

- #### Informacion de los datos para importar en Odoo
<img src="imagenes/Tarea/1.1.PNG" width="500" name="Odoo datos para importar">
<br>

- #### Informacion de los datos importados en Odoo
<img src="imagenes/Tarea/1.2.PNG" width="500" name="Datos importados">
<br>

2.0 Presentacion mejorada
- #### Header
<img src="imagenes/Tarea/2.0.PNG" width="500" name="Header">
<br>

- #### Body
<img src="imagenes/Tarea/2.1.PNG" width="500" name="Body">
<br>

- #### Footer
<img src="imagenes/Tarea/2.2.PNG" width="500" name="Footer">
<br>

## Conclusiones

- Al instalar el CRM debemos poner cuales van a ser nuestros objetivos, kpis o nuestras oportunidades
- Podemos crear varios clientes y personalizarlos de manera facil porque Odoo nos brinda una interfaz amigable
- Al igual forma que lo anterior se puede crear miembros y contactos
- Generar iniciativas para nuestra empresa es muy importante porque estas pueden darnos grandes resultados con las ventas, reunione o mas trabajo
- Podemos tambien personalizar nuestro sitio web con diferentes temas o nuevos temas que creamos
