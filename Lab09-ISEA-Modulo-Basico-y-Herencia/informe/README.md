# Lab09: MÓDULO BÁSICO Y HERENCIA

## Desarrollo

### 3.0 Creación de módulo Facturación
- #### 3.5 Imagen de la tabla facturacion_series
<img src="imagenes/3.5.PNG" width="520" name="Tabla facturas">
<br>

- #### 3.6 Imagen del menuitem Series dentro de Peru
<img src="imagenes/3.6.PNG" width="520" name="MenuItem - Peru">
<br>

- #### 3.7 GIF del funcionamiento del modulo
<img src="imagenes/3.7.gif" width="520" name="Funcionamiento">
<br>

### 4.0 Vista de formulario
- #### 4.2 GIF del funcionamiento del nuevo formulario
<img src="imagenes/4.2.gif" width="520" name="Funcionamiento">
<br>

### 5.0 Vista de búsqueda
- #### 5.2 GIF del formulario con su nueva busqueda
<img src="imagenes/5.2.gif" width="520" name="Formulario busqueda">
<br>

### 6.0 Data por defecto
- #### 6.3 Imagen de la lista importada
<img src="imagenes/6.3.PNG" width="520" name="Importado">
<br>

### 7.0 Verificación de API automática
- #### 7.3 GIF de la ejecucion del comando
<img src="imagenes/7.3.gif" width="520" name="Verificacion">
<br>

### 8.0 Herencia de modelos
- #### 8.5 GIF de la creacion de la factura
<img src="imagenes/8.5.gif" width="520" name="Creacion">
<br>

## Tarea 

- #### 9.1 GIF Mostrando la funcionalidad del modelo facturacion.documentos
<img src="imagenes/9.1.gif" width="520" name="Funcionalidad">
<br>

- #### 9.2 Dato importado al modelo
<img src="imagenes/9.2.PNG" width="520" name="Dato">
<br>

## Conclusiones

- Cuando creamos una vista para el módulo, no es necesario reiniciar el servidor, pero cuando actualizamos un archivo .py, tenemos que hacerlo para que lo compile
- Para poder usar los modelos que vayamos creando, tenemos que inicializarlo en el archivo .init
- Cuando creamos un creamos cualquier modelo, Odoo automáticamente hace un CRUD en este dónde podemos modificar los datos ya que se crea automáticamente la tabla
- En este laboratorio el selector está archivando el elemento si lo desmarcamos, no aparecerá en la lista de Series por eso tenemos que dejarlo marcado
- Odoo tiene su propia API automática donde podemos conectarnos de forma fácil (nuestras credenciales odoo) y hacer las peticiones 


