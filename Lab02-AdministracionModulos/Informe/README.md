# Lab. 2: Administración de Módulos en ODOO v3

## Desarrollo

3.6 **Apps instalados** y muestra con filtros
- #### Imagen de filtros
<img src="Imagenes/Windows/3.6.1.PNG" width="500" name="Imagen de filtros">
<br>

- #### Módulos instalados
<img src="Imagenes/Windows/3.6.2.PNG" width="500" name="Módulos instalados">
<br>

4.0 Identificación del **Tablero** de opciones
```
1. Módulo de Gestión de Ausencias
2. Modulo Punto de venta
3. Modulo Gestión de ventas
```

4.5 Modo desarrollador activado
- #### Odoo 11.0-20190814
![modo_desarrolador image](Imagenes/Windows/4.5.PNG)

5.0 Creación de un **modulo en ODOO**
- #### Windows
	- **Archivos testmodulo2**
	![archivo_tstmd2 image](Imagenes/Windows/5.0.PNG)
	
	- **Testmodulo2 Instalado**
    ![archivo_tstmd2 image](Imagenes/Windows/5.1.PNG)

- #### Ubuntu
	- **Archivos testmodulo1**
	![archivo_tstmd2 image](Imagenes/Ubuntu/5.2.PNG)
	
	- **Testmodulo1 Instalado**
    ![archivo_tstmd2 image](Imagenes/Ubuntu/5.3.PNG)

## Tarea
<img src="Imagenes/Tarea.PNG" width="500" name="Tarea imagen">
<br>

## Conclusiones

- Podemos filtrar los datos de las aplicaciones o módulos para que sea más fácil encontrar lo que estamos buscando
- Cada app que instalamos tiene su propio tablero configurado donde estarán las distintas herramientas que posee
- El modo desarrollador nos permite Actualizar las apps que estamos creando, además de solo agregar un debug en el url
- Con los módulos que vamos a crear nos permitirán adicionar más herramientas o crear apps para completar el proyecto dado
- Los módulos que vamos a crear se guardan en la carpeta addons donde se guardaran internamente en Odoo ya que si lo borramos, igual estará activo en el sistema
